<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource("home", "HomeController");

Route::resource("tasks", "TasksController", ['except' => ["update","destroy"]]);
Route::resource("settings", "SettingController", ['except' => ["create","show","destroy"]]);

Route::resource("admin/list", "AdminTasksController");
Route::resource("admin/users", "AdminUsersController");
