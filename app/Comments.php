<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comments extends Model
{
    protected $fillable = ['user_id', 'text', 'created_at'];
}
