<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\User;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        date_default_timezone_set('Europe/Moscow');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('settings');
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->input("name");
        $user->company = $request->input("company");
        $user->site = $request->input("site");
        $user->updated_at = date('Y-m-d H:i:s');

        $user->save();  
        return redirect()->route("settings.index");
    }
}
