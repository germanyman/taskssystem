<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\User;
use App\Tasks;
use App\Comments;
use App\Files;

class AdminUsersController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Europe/Moscow');
        $this->middleware('admin');
    }

    public function index()
    {
        $users = User::all();
        return view('admin.users')->withusers($users);
    }

    
    public function store(Request $request){
        $this->validate($request, array(
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|string|min:6',
            'company' => 'required|max:255',
            'site' => 'required|max:255',
            'phone' => 'required|max:15',

        ));

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->company = $request->company;
        $user->site = $request->site;
        $user->phone = $request->phone;
        $user->updated_at = date('Y-m-d H:i:s');
        $user->save();
        return Redirect::route('users.index');
    }

    public function update(Request $request){
        $this->validate($request, array(
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'company' => 'required|max:255',
            'site' => 'required|max:255',
            'phone' => 'required|max:15',
        ));

        $user = User::findOrFail($request->user);
        $user->name = $request->input("name");
        $user->email = $request->input("email");
        $user->company = $request->input("company");
        $user->phone = $request->input("phone");
        $user->site = $request->input("site");
        $user->updated_at = date('Y-m-d H:i:s');
        $user->save();
        return Redirect::to('admin/users/'.$request->user);

    }
    public  function show(Request $request){
        $user = DB::table('users')
            ->whereId($request->user)
            ->first();
        return view('admin.user')->withuser($user);
    }

    public function destroy(Request $request, $id) {
        User::destroy($id);
        return Redirect::to('admin/users/');
    }
}
