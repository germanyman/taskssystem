<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Tasks;
use App\User;
use App\Comments;
use App\Files;
use DB;

class TasksController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Europe/Moscow');
        $this->middleware('auth');
    }

    public function index()
    {   
        $tasks = DB::table('tasks')
            ->where('user_id', Auth::user()->id)
            ->get();
        return view('tasks')->withtasks($tasks);
    }

    public function edit(Request $request)
    {
        if (isset($request->sendComment)) {
            $comment = new Comments;
            $comment->user_id = Auth::user()->id;
            $comment->task_id = $request->task;
            $comment->text = $request->comment;
            $comment->updated_at = date('Y-m-d H:i:s');
            $comment->save();
        }
        return Redirect::to('tasks/'.$request->task);

    }
    public  function show(Request $request){
        $task = DB::table('tasks')
            ->whereId($request->task)
            ->first();
        $adminName = DB::table('users')->whereId($task->admin_id)->first();
        $comments = DB::table('comments')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.task_id', $request->task)
            ->get();
        $files = DB::table('files')
            ->where('task_id', $request->task)
            ->get();
        return view('task')->withtask($task)->withinfo($adminName)->withcomments($comments)->withfiles($files);
    }
    public function store(Request $request){
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required|max:255|unique:users,email',
        ));

        $task = new Tasks;
        $task->user_id = Auth::user()->id;
        $task->task_name = $request->title;
        $task->task_text = $request->description;
        $task->save();

        if( $request->file ) {
            $file = $request->file;
            $fileName = time().'.'.$file->getClientOriginalName();
            $request->file->move(public_path('img'), $fileName);
            $fileSend = new Files([
                'task_id' => $task->id,
                'link' => $fileName
                ]);
            $fileSend->save();
        }           
        return Redirect::route('tasks.index');
    }
}
