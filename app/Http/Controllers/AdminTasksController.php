<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\User;
use App\Tasks;
use App\Comments;
use App\Files;

class AdminTasksController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Europe/Moscow');
        $this->middleware('admin');
    }

    public function index()
    {
        $tasks = Tasks::all();
        return view('admin.tasks')->withtasks($tasks);
    }

    public function edit(Request $request){
        if (isset($request->sendComment)) {
            $comment = new Comments;

            $comment->user_id = Auth::user()->id;
            $comment->task_id = $request->list;
            $comment->text = $request->comment;
            $comment->updated_at = date('Y-m-d H:i:s');
            $comment->save();
        }
        return Redirect::to('admin/list/'.$request->list);
    }
    public function update(Request $request){
        if (isset($request->updTask)) {
            $task = Tasks::findOrFail($request->list);
            $task->task_name = $request->input("title");
            $task->task_status = $request->input("status_id");
            $task->task_text = $request->input("text");
            $task->updated_at = date('Y-m-d H:i:s');
            $task->save();

            if( $request->file ) {
                $file = $request->file;
                $fileName = time().'.'.$file->getClientOriginalName();
                $request->file->move(public_path('img'), $fileName);
                $fileSend = new Files([
                    'task_id' => $request->list,
                    'link' => $fileName
                    ]);
                $fileSend->save();
            }           
        }
        if (isset($request->delFile)) {
            $file = DB::table('files')
            ->where('id', $request->file_id)
            ->first();
            @unlink('img/'.$file->link);
            Files::destroy($request->file_id);
        }     
        return Redirect::to('admin/list/'.$request->list);
    }
    public  function show(Request $request){
        $task = DB::table('tasks')->whereId($request->list)->first();

        $taskUpdStatus = Tasks::findOrFail($request->list);
        if ($taskUpdStatus->task_status < 2) {
            $taskUpdStatus->task_status = 2;
            $taskUpdStatus->admin_id = Auth::user()->id;
            $taskUpdStatus->save();
        }
        $userName = DB::table('users')->whereId($task->user_id)->first();
        $comments = DB::table('comments')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.task_id', $request->list)
            ->get();
        $files = DB::table('files')
            ->where('task_id', $request->list)
            ->get();
        return view('admin.task')->withtask($task)->withinfo($userName)->withcomments($comments)->withfiles($files);
    }

    public function store(Request $request){
        $this->validate($request, array(
            'title' => 'required|max:255',
            'description' => 'required|max:255',
        ));

        $task = new Tasks;
        $task->user_id = Auth::user()->id;
        $task->task_name = $request->title;
        $task->task_text = $request->description;
        $task->save();
        if( $request->file ) {
            $file = $request->file;
            $fileName = time().'.'.$file->getClientOriginalName();
            $request->file->move(public_path('img'), $fileName);
            $fileSend = new Files([
                'task_id' => $task->id,
                'link' => $fileName
                ]);
            $fileSend->save();
        }    
        return Redirect::to('admin/list');
    }
    public function destroy(Request $request, $task_id)
    {
        $task = Tasks::find($task_id);
        $task->task_status = 5;
        $task->updated_at = date('Y-m-d H:i:s');
        $task->save();
        return Redirect::to('admin/list');
    }
}
