<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Exceptions\UnauthorizedException;

class AllowIfAdmin
{

    public function handle($request, Closure $next)
    {
        if (Auth::check()) {

            if (Auth::user()->isAdmin == 1) {

                return $next($request);
            }

            return redirect('welcome');

        }

        return redirect('welcome');
    }
}
