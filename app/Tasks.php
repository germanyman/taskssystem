<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Tasks extends Model
{
    protected $fillable = ['user_id', 'task_name', 'task_text', 'task_status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
