# TasksSystem ~ Laravel 5.5

## Установка

Первое, что необходимо сделать, заполнить необходимые поля данными текущего рабочего окружения. В репозитории находится файл .env.example. Для работы приложения вам необходимо переименовать .env.example файл в .env и заполнить а нем необходимые данные, или создать пустой файл .env и вставить в него следующее:

~~~~

APP_ENV=local
APP_KEY=generate-your-app-key
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=log
MAIL_HOST="mailtrap.io"
MAIL_PORT=2525
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=

~~~~ 

Далее необходимо установить все необходимые зависимости. Выполните следующее в коммандной строке:

~~~~

composer install

~~~~

Оставьте поле APP_KEY пустым. Для его автозаполнения запустите следующую команду:

~~~~

php artisan key:generate

~~~~

Вам следует заполнить поля для подключания к базе данных в .env файле:

~~~~

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your-db-name
DB_USERNAME=your-db-username
DB_PASSWORD=your-db-password

~~~~

После заполнения данных для подключения к БД и установки записимостей, выполните следующую команду, которая создаст все необходимые поля в БД:

~~~~

php  artisan migrate

~~~~

Проект готов к запуску. 

P.S. Любой зарегистрированный пользователь не имеет доступа к админ панели. Чтобы предоставить первому админу все права, необходит в таблице users изменить значения поля isAdmin c 0 на 1.