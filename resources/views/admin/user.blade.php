@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="profile-userpic">
                    <img src="{{asset(Auth::user()->photo)}}" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        @if (Auth::user()->isAdmin == 0)
                            Пользователь
                        @endif
                    </div>
                </div>
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li>
                            <a href="/home">
                            <i class="glyphicon glyphicon-home"></i>
                            Profile </a>
                        </li>
                        <li>
                            <a href="/admin/list">
                            <i class="glyphicon glyphicon-list"></i>
                            Tasks </a>
                        </li>
                        <li class="active">
                            <a href="/admin/users">
                            <i class="glyphicon glyphicon-eye-open"></i>
                            Users </a>
                        </li>
                        <li>
                            <a href="/settings">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
                {!! Form::open(['route' => ["users.update", $user->id], 'data-parsley-validate' => '','files'=> 'true', 'enctype' => "multipart/form-data", 'method' => 'PUT']) !!}
                {{ Form::label('name', "Name:") }}
                {{ Form::text("name", $user->name, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                
                {{ Form::label('company', "Company:") }}
                {{ Form::text("company", $user->company, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                {{ Form::label('site', "Site:") }}
                {{ Form::text("site", $user->site, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                {{ Form::label('phone', "Phone:") }}
                {{ Form::text("phone", $user->phone, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                
                {{ Form::label('email', "Email:") }}
                {{ Form::email("email", $user->email, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                {{ Form::submit("Update User" , array('class' => "btn btn-success btn-block", "style" => "margin-top:20px;", "name" => "updTask")) }}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){  
        $("#addTask").on('click', function(){
            $('#formAdd').show();
            $("#addTask").hide();
            $("#closeTask").show();
        });
        $("#closeTask").on('click', function(){
            $('#formAdd').hide();
            $("#addTask").show();
            $("#closeTask").hide();
        });
    });
</script>
@endsection