@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="profile-userpic">
                    <img src="{{asset(Auth::user()->photo)}}" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        @if (Auth::user()->isAdmin == 0)
                            Пользователь
                        @endif
                    </div>
                </div>
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li>
                            <a href="/home">
                            <i class="glyphicon glyphicon-home"></i>
                            Profile </a>
                        </li>
                        <li class="active">
                            <a href="/admin/list">
                            <i class="glyphicon glyphicon-list"></i>
                            Tasks </a>
                        </li>
                        <li>
                            <a href="/admin/users">
                            <i class="glyphicon glyphicon-eye-open"></i>
                            Users </a>
                        </li>
                        <li>
                            <a href="/settings">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-6">
                            <b>Name_Task</b>
                        </div>
                        <div class="col-md-6">
                            {{$task->task_name}}
                        </div>
                        <div class="col-md-6">
                            <b>Description_Task</b>
                        </div>
                        <div class="col-md-6">
                            {{$task->task_text}}
                        </div>
                        <div class="col-md-6">
                            <b>Status</b>
                        </div>
                        <div class="col-md-6">@if ($task->task_status == 1)
                                 <span class="label label-primary">New</span>
                            @elseif ($task->task_status == 2)
                                <span class="label label-warning">Viewed</span>
                            @elseif ($task->task_status == 3)
                                <span class="label label-info">Paid</span>
                            @elseif ($task->task_status == 4)
                                <span class="label label-success">Success</span>
                            @elseif ($task->task_status == 5)
                                <span class="label label-danger">Closed</span>
                            @endif
                            
                        </div>
                        <div class="col-md-6"><b>Creater</b></div>
                        <div class="col-md-6">
                            @if (empty($info->name))
                                Не назначен
                            @else
                                {{ $info->name }}
                            @endif
                        </div>
                        <div class="col-md-6">
                            <b>Created_at</b>
                        </div>
                        <div class="col-md-6">
                            {{$task->created_at}}
                        </div>
                        <div class="col-md-6">
                            <b>Updated_at</b>
                        </div>
                        <div class="col-md-6">
                            {{$task->updated_at}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        {!! Form::open(['route' => ["list.update", $task->id], 'data-parsley-validate' => '','files'=> 'true', 'enctype' => "multipart/form-data", 'method' => 'PUT']) !!}
                        {{ Form::label('title', "Title:") }}
                        {{ Form::text("title", $task->task_name, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                        {{ Form::label('status', "Status:") }}
                        {{ Form::hidden("status", $task->task_status, array("id"=> "status_current")) }}
                        <select class="form-control" name="status_id" id="status">
                            <option value="1">New</option>
                            <option value="2">Viewed</option>
                            <option value="3">Paid</option>
                            <option value="4">Success</option>
                            <option value="5">Closed</option>
                        </select>

                        {{ Form::label('file', "Upload Featured file:") }}
                        {{ Form::file('file') }}

                        {{ Form::label('text', "Text Task:") }}
                        {{ Form::textarea('text', $task->task_text, array("class"=> "form-control")) }}

                        {{ Form::submit("Update Task" , array('class' => "btn btn-success btn-block", "style" => "margin-top:20px;", "name" => "updTask")) }}
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-12" style="margin-top:20px;">
                        <p align="center"><b>Files</b></p>
                        <hr>
                        @if ($files->count() != 0)
                            <table class="table table-striped table-hover ">
                                <thead>
                                    <tr>
                                        <th>Link</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($files as $file)
                                        <td><a href="{{ asset( $file->link ) }}"> {{ $file->link }}</a></td>
                                        <td>
                                            {!! Form::open(["route" => ['list.update', $task->id], "method" => "PUT"]) !!}
                                            {{ Form::hidden("file_id", $file->id) }}
                                            {{ Form::submit("Delete", ['class'=>'btn btn-default', 'name' => 'delFile' ])}}
                                            {!! Form::close() !!}
                                        </td>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="col-md-12">
                                <p align="center">not Files</p>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <p align="center"><b>Comments</b></p>
                            <hr>
                            @if ($comments->count() != 0)
                                <div style="overflow:auto; max-height: 300px">
                                        @foreach ($comments as $comment)
                                            <div class="col-md-2">
                                                <div class="thumbnail">
                                                    <img class="img-responsive user-photo" width="32px" src="{{ asset($comment->photo) }}">
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <strong>{{ $comment->name }}</strong> <span class="pull-right">{{ $comment->created_at }}</span>
                                                    </div>
                                                    <div class="panel-body">
                                                        {{ $comment->text }}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                </div>
                            @else
                                <div class="col-md-12">
                                    <p align="center">not Comment</p>
                                </div>
                            @endif

                            <div style="margin-top:10px">
                                {!! Form::open(['route' => ['list.edit', $task->id], 'data-parsley-validate' => '',"method" => "GET"]) !!}
                                {{ Form::textarea('comment', null, array("class"=> "form-control", "maxlength"=> "140", "rows" => "4")) }}
                                {{ Form::submit("Send Comment" , array('class' => "btn btn-success btn-block", "style" => "margin-top:20px;", "name" => "sendComment")) }}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var status = $("#status_current").val();
    $("#status [value="+status+"]").attr("selected", "selected");
</script>
@endsection
