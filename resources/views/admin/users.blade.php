@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="profile-userpic">
                    <img src="{{asset(Auth::user()->photo)}}" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        @if (Auth::user()->isAdmin == 0)
                            Пользователь
                        @endif
                    </div>
                </div>
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li>
                            <a href="/home">
                            <i class="glyphicon glyphicon-home"></i>
                            Profile </a>
                        </li>
                        <li>
                            <a href="/admin/list">
                            <i class="glyphicon glyphicon-list"></i>
                            Tasks </a>
                        </li>
                        <li class="active">
                            <a href="/admin/users">
                            <i class="glyphicon glyphicon-eye-open"></i>
                            Users </a>
                        </li>
                        <li>
                            <a href="/settings">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
                <a type="button" id="addTask" class="btn btn-success">Add User</a>
                <a type="button" id="closeTask" class="btn btn-warning" style="display:none">Close Form</a>
                {!! Form::open(['route' => "users.store", 'data-parsley-validate' => '','method' => 'POST', "style" => "display:none;","id"=>"formAdd"]) !!}
                {{ Form::label('name', "Name:") }}
                {{ Form::text('name', null, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                
                {{ Form::label('company', "Company:") }}
                {{ Form::text('company', null, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                {{ Form::label('site', "Site:") }}
                {{ Form::text('site', null, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                {{ Form::label('phone', "Phone:") }}
                {{ Form::text('phone', null, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                
                {{ Form::label('email', "Email:") }}
                {{ Form::email('email', null, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                {{ Form::label('password', "Password:") }}
                {{ Form::password('password', null, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}

                {{ Form::submit("Add User" , array('class' => "btn btn-success btn-block", "style" => "margin-top:20px;", "name" => "updUsers")) }}
                {!! Form::close() !!}
                
                <table class="table table-striped table-hover ">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>FIO</th>
                        <th>Company</th>
                        <th>Site</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Open</th>
                        <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->company }}</td>
                                <td>{{ $user->site }}</td>
                                <td>{{ $user->phone }}</td>
                                @if ($user->isAdmin == 1)
                                     <td><span class="label label-primary">Admin</span></td>
                                @else
                                    <td><span class="label label-warning">User</span></td>
                                @endif
                                <td><a href="../admin/users/{{ $user->id }}" class="btn btn-default">Open</a></td>
                                <td>
                                    {!! Form::open(["route" => ['users.destroy', $user->id], "method" => "DELETE"]) !!}
                                    {{  Form::submit("Delete", ['class'=>'btn btn-default'])}}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){  
        $("#addTask").on('click', function(){
            $('#formAdd').show();
            $("#addTask").hide();
            $("#closeTask").show();
        });
        $("#closeTask").on('click', function(){
            $('#formAdd').hide();
            $("#addTask").show();
            $("#closeTask").hide();
        });
    });
</script>
@endsection