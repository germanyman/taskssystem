@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="profile-userpic">
                    <img src="{{asset(Auth::user()->photo)}}" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        @if (Auth::user()->isAdmin == 0)
                            Пользователь
                        @endif
                    </div>
                </div>
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li>
                            <a href="/home">
                            <i class="glyphicon glyphicon-home"></i>
                            Profile </a>
                        </li>
                        <li class="active">
                            <a href="{{ Auth::user()->isAdmin == 1  ? '/admin/list' : '/tasks' }}">
                            <i class="glyphicon glyphicon-list"></i>
                            Tasks </a>
                        </li>
                        <li>
                            <a href="/settings">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
                <a type="button" id="addTask" class="btn btn-success">Add Task</a>
                <a type="button" id="closeTask" class="btn btn-warning" style="display:none">Close Form</a>
                {!! Form::open(['route' => "tasks.store", 'data-parsley-validate' => '','files'=> 'true',"method" => "POST","style" => "display:none;","id"=>"formAdd"]) !!}
                {{ Form::label('title', "Title:") }}
                {{ Form::text("title", null, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                {{ Form::label('body', "Description:") }}
                {{ Form::textarea('description', null, array("class"=> "form-control")) }}
                {{ Form::label('file', "Upload Featured file:") }}
                {{ Form::file('file') }}
                {{ Form::submit("Add" , array('class' => "btn btn-success btn-block", "style" => "margin-top:20px;")) }}
                {!! Form::close() !!}
                <table class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Open</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $task->task_name }}</td>
                                @if ($task->task_status == 1)
                                     <td><span class="label label-primary">New</span></td>
                                @elseif ($task->task_status == 2)
                                    <td><span class="label label-warning">Viewed</span></td>
                                @elseif ($task->task_status == 3)
                                    <td><span class="label label-info">Paid</span></td>
                                @elseif ($task->task_status == 4)
                                    <td><span class="label label-success">Success</span></td>
                                @elseif ($task->task_status == 5)
                                    <td><span class="label label-danger">Closed</span></td>
                                @endif
                                <td>{{ $task->created_at }}</td>
                                <td>{{ $task->updated_at }}</td>
                                <td><a href="../tasks/{{$task->id}}" class="btn btn-default">Open</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){  
        $("#addTask").on('click', function(){
            $('#formAdd').show();
            $("#addTask").hide();
            $("#closeTask").show();
        });
        $("#closeTask").on('click', function(){
            $('#formAdd').hide();
            $("#addTask").show();
            $("#closeTask").hide();
        });
    });
</script>
@endsection
