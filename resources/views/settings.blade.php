@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <div class="profile-userpic">
                    <img src="{{asset(Auth::user()->photo)}}" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        @if (Auth::user()->isAdmin == 0)
                            Пользователь
                        @endif
                    </div>
                </div>
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li>
                            <a href="/home">
                            <i class="glyphicon glyphicon-home"></i>
                            Profile </a>
                        </li>
                        <li>
                            <a href="{{ Auth::user()->isAdmin == 1  ? '/admin/list' : '/tasks' }}">
                            <i class="glyphicon glyphicon-list"></i>
                            Tasks </a>
                        </li>
                        @if (Auth::user()->isAdmin == 1)
                        <li>
                            <a href="/admin/users">
                            <i class="glyphicon glyphicon-eye-open"></i>
                            Users </a>
                        </li>
                        @endif
                        <li class="active">
                            <a href="/settings">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        {!! Form::open(['route' => ["settings.edit", Auth::user()->id], 'data-parsley-validate' => '',"method" => "GET"]) !!}
                        {{ Form::label('name', "Name") }}
                        {{ Form::text("name", Auth::user()->name, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                        {{ Form::label('title', "Company:") }}
                        {{ Form::text("company", Auth::user()->company, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                        {{ Form::label('title', "Site:") }}
                        {{ Form::text("site", Auth::user()->site, array("class"=> "form-control", 'required' => "", 'maxlength' => "255")) }}
                        {{ Form::submit("Edit" , array('class' => "btn btn-success btn-block", "style" => "margin-top:20px;")) }}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
