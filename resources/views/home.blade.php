@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    
    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="{{asset(Auth::user()->photo)}}" class="img-responsive" alt="">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        @if (Auth::user()->isAdmin == 0)
                            Пользователь
                        @endif
                    </div>
                </div>
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="/home">
                            <i class="glyphicon glyphicon-home"></i>
                            Profile </a>
                        </li>
                        <li>
                            <a href="{{ Auth::user()->isAdmin == 1  ? '/admin/list' : '/tasks' }}">
                            <i class="glyphicon glyphicon-list"></i>
                            Tasks </a>
                        </li>
                        @if (Auth::user()->isAdmin == 1)
                        <li>
                            <a href="/admin/users">
                            <i class="glyphicon glyphicon-eye-open"></i>
                            Users </a>
                        </li>
                        @endif
                        <li>
                            <a href="/settings">
                            <i class="glyphicon glyphicon-user"></i>
                            Account Settings </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
               <b>Name</b> - {{ Auth::user()->name }} <br>
               <b>Company</b> - {{ Auth::user()->company }}<br>
               <b>Site</b> - {{ Auth::user()->site }}<br>
               <b>Email</b> - {{ Auth::user()->email }}<br>
            </div>
        </div>
    </div>
</div>
@endsection
